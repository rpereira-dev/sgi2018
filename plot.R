
require(dplyr)
require(ggplot2)

#--------------------------------------------------------------------------------------------------
# IMPORT DATA

folder <- "output"
pathEigenvalues <- paste(folder, "phase_eigenvalues.csv", sep='/')
pathMontecarlo <- paste(folder, "phase_montecarlo.csv", sep='/')
pathXi <- paste(folder, "xi.csv", sep='/')

eigenvalues <- data.table::fread(pathEigenvalues)
montecarlo <- data.table::fread(pathMontecarlo)

xi <- data.table::fread(pathXi)
xi <- complex(real = xi$V1, imaginary = xi$V2)

#--------------------------------------------------------------------------------------------------
# ALPHA

# alpha used to reflect distance to xi=0
alpha <- 1 / (1 + Mod(xi)) 
minalpha <- 0.2
alpha <- ifelse(alpha > minalpha, alpha, minalpha)

#--------------------------------------------------------------------------------------------------
# PLOT DATA

# model that will be plotted
model <- montecarlo

# plot axis
plot <- ggplot() +
   theme_light() +
   labs(x = "θ · 𝕽[λ]^½", y="1 / 𝕴[λ]^½") +
   coord_cartesian(xlim = c(353.5, 361.5), ylim = c(2.825e-5, 2.875e-5))

# plots eigenvalues in the background
for(i in 1:(ncol(eigenvalues)/2))
{
   j <- 2*i - 1
   data <- eigenvalues[,j:(j+1)]
   colnames(data) <- c("x", "y")
   plot <- plot + geom_point(data=data, aes(x=x, y=y), colour = 'black', alpha=alpha, size = alpha)
}

# plot tracked eigenvalues
nbTarget <- ncol(model)/2
model_colors <- rainbow(nbTarget)
for(i in 1:nbTarget)
{
   j <- 2*i - 1
   data <- model[,j:(j+1)]
   colnames(data) <- c("x", "y")
   plot <- plot + geom_point(data=data, aes(x=x, y=y), colour = model_colors[i], alpha=alpha, size = 0.5 + alpha)
}

X11()
print(plot)

plot(montecarlo)

n <- 9

reel <- cbind(montecarlo[,1], montecarlo[,3], montecarlo[,5], montecarlo[,7], montecarlo[,9], montecarlo[,11], montecarlo[,13], montecarlo[,15], montecarlo[,17])
imag <- cbind(montecarlo[,2], montecarlo[,4], montecarlo[,6], montecarlo[,8], montecarlo[,10], montecarlo[,12], montecarlo[,14], montecarlo[,16], montecarlo[,18])


reel <- data.frame(reel)
imag <- data.frame(imag)

for (i in 1:n) {
   plot(density(as.numeric(reel[,i])), main = "Partie réelle de la valeur propre")
   plot(density(as.numeric(imag[,i])), main = "Partie imaginaire de la valeur propre")
   }


mean_reel <- c(1:n)
mean_imag <- c(1:n)

sd_reel <- c(1:n)
sd_imag <- c(1:n)


for (i in 1:n) {
   mean_reel[i] <- mean(reel[,i])
   mean_imag[i] <- mean(imag[,i])
   sd_reel[i] <- sd(reel[,i])
   sd_imag[i] <- sd(imag[,i])
}

test_reel <- c(1:n)
test_imag <- c(1:n)

for (i in 1:n) {
   test_reel[i] <- floor(-log10(abs(sd_reel[i]/mean_reel[i])))
   test_imag[i] <- floor(-log10(abs(sd_imag[i]/mean_imag[i])))
}


Mclust(imag[,4])

