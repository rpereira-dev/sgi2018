#pragma once

#include <queue>
#include <cfloat>
#include <iostream>
#include "../types.h"
#include "point.h"
#include "follow.h"

extern "C" {
#include "pqueue.h"
}

namespace Transform
{
    // takes a starting point, the indexes of the target eigenvalues in the starting point and a vector of parameters to generate ending points
    // follows the target from the starting point to each of the ending points, independently
    // returns a matrix with a row per xi and a column per target
    ComplexMatrix linearPath(const Point& startPoint, const std::vector<unsigned int>& startIndexes, const ComplexVector& xiVector,
                             const number distanceThreshold = 0.05, const number minstepsize = std::sqrt(3.)*1e-6)
    {
        // NOTE:
        // to generate a point from a parameter i, you can use  :
        // Point point(startPoint.omega, startPoint.size, xiVector[i]);
        //
        // to follow several targets from a point to another, you can use :
        // std::vector<unsigned int> indexesResult = Follow::adaptativStep(startPoint, endPoint, startIndexes, distanceThreshold, minstepsize);
        // throw std::logic_error("Function not yet implemented.");
        //
        // ComplexMatrix(row, col) (row == xi ; col == )
        ComplexMatrix mat(xiVector.size(), startIndexes.size());

        # pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0 ; i < xiVector.size() ; i++) {
            // the target
            Point endPoint(startPoint.omega, startPoint.size, xiVector[i]);
            std::vector<unsigned int> endIndexes = Follow::adaptativStep(startPoint, endPoint, startIndexes, distanceThreshold, minstepsize);
            for (unsigned int j = 0 ; j < endIndexes.size() ; j++) {
                mat(i, j) = endPoint.formatedEigenvalues[endIndexes[j]];
            }
        }
        return mat;
    }

    // takes a starting point, the indexes of the target eigenvalues in the starting point and a vector of parameters to generate ending points
    // follows the target from the starting point to each of the ending points, independently
    // returns a matrix with a row per xi and a column per target
    // NOTE: this function exports the phase speeds of all the eigenvalues to a csv file for later analysis
    ComplexMatrix linearPath(const Point& startPoint, const std::vector<unsigned int>& startIndexes, const ComplexVector& xiVector,
                             Csv& outCsv, const number distanceThreshold = 0.05, const number minstepsize = std::sqrt(3.)*1e-6)
    {
        // NOTE: identical to the previous function but dumps all eigenvalues produced, not just targets, to outCsv
        //
        // to output the eigenvalues of a point, you can use :
        // outCsv.write( (ComplexVector) phaseSpeed(startPoint.omega, point.formatedEigenvalues) );
        //
        // WARNING: you will need some sort of locking mechanism to export values while running in parallel
        ComplexMatrix mat(xiVector.size(), startIndexes.size());
        # pragma omp parallel for schedule(dynamic)
        for (unsigned int i = 0 ; i < xiVector.size() ; i++) {
            // the target
            Point endPoint(startPoint.omega, startPoint.size, xiVector[i]);
            std::vector<unsigned int> endIndexes = Follow::adaptativStep(startPoint, endPoint, startIndexes, distanceThreshold, minstepsize);
            for (unsigned int j = 0 ; j < endIndexes.size() ; j++) {
                mat(i, j) = endPoint.formatedEigenvalues[endIndexes[j]];
            }
            // dump endPoint.formatedEigenvalues
            #pragma omp critical
	    {
                outCsv.write( (ComplexVector) phaseSpeed(startPoint.omega, endPoint.formatedEigenvalues) );
            }
        }
        return mat;
    }

    // A node of the graph for the 'treePath' implementation
    class Node {

    public:
        Point point;
        number cost;
        Node * pred;
        std::vector<unsigned int> indexes;
        std::vector<Node *> succs;

        Node() {}
        Node(const Point & startPoint, const complexNumber& xi) : 
            point(startPoint.omega, startPoint.size, xi),
            cost(),
            pred(NULL),
            indexes(),
            succs() {}
        ~Node() {}
    };

    static int number_cmpf(void const * left, void const * right) {
        number x = *((number *)left);
        number y = *((number *)right);
        return (x < y) ? -1 : (x > y) ? 1 : 0;
    }

    // takes a starting point, the indexes of the target eigenvalues in the starting point and a vector of parameters to generate ending points
    // returns a matrix with a row per xi and a column per target
    //
    // to do so, this function builds a minimum spanning tree that is processed inparallel
    // the tree has xi=(0,0) as its root (the starting point) and goes to all the values inside xiVector
    ComplexMatrix treePath(const Point& startPoint, const std::vector<unsigned int>& startIndexes, const ComplexVector& xiVector, Csv& outCsv,
                           const number distanceThreshold = 0.05, const number minstepsize = std::sqrt(3.)*1e-6)
    {
        // FACULTATIV
        // NOTE: this function can be tricky to implement without using too much memory for large number of points or having an n² complexity
        // you might want to play with building the tree before processing it or processing it as you are building it
        // the fastest implementation gets a bonus
        // throw std::logic_error("Function not yet implemented.");
        //
        // The implementation follows https://en.wikipedia.org/wiki/Prim%27s_algorithm
 
        // Prim's algorithm : https://fr.wikipedia.org/wiki/Algorithme_de_Prim
	
        // create each nodes and compute eigen values
        unsigned int m = startIndexes.size();
	unsigned int n = xiVector.size();
        pqueue_t * F = pqueue_new(number_cmpf);
        std::vector<Node> nodes(n + 1);

        // insert root node
        nodes[n] = Node(startPoint, startPoint.xi);
        Node * startNode = &(nodes.at(n));
        startNode->indexes = startIndexes;
        startNode->cost = 0.0;
        pqueue_insert(F, &(startNode->cost), startNode);

        # pragma omp parallel for schedule(static)
	for (unsigned int i = 0 ; i < n ; i++) {
            nodes[i] = Node(startPoint, xiVector[i]);
            Node * node = &(nodes.at(i));
            node->cost = Point::squareDistance(startPoint, node->point);
            # pragma omp critical
            {
                pqueue_insert(F, &(node->cost), node);
            }
        }

        // empty the priority queue
        std::vector<pqueue_node_t *> decreased;
        while (!pqueue_is_empty(F)) {
            pqueue_node_t pqnode = pqueue_pop(F);
            Node * t = (Node *)pqnode.value;
            # pragma omp parallel for schedule(static)
            PQUEUE_ITERATE_START(F, Node *, u, i) {
                number distance = Point::squareDistance(t->point, u->point);
                if (u->cost >= distance) {
                    # pragma omp critical
                    {
                        u->pred = t;
                        u->cost = distance;
                        decreased.push_back(__pqueue_node_current__);
                    }
                }
            }
            PQUEUE_ITERATE_STOP(F, Node *, u, i);
           
            // only update the priority queue after the iterator ended
            // to avoid concurrency issues 
            for (auto & pqnode : decreased) {
                Node * node = (Node *)pqnode->value;
                pqueue_decrease(F, pqnode, &(node->cost));
            }
            decreased.clear();
        }

        // no longer need the priority queue
        pqueue_delete(F);

        // fill the 'succs' list to prepare the breadth first search
        for (unsigned int i = 0; i < n ; i++) {
            Node * node = &(nodes.at(i));
            if (node->pred) {
                node->pred->succs.push_back(node);
            }
        }

        // Breadth first search to go through each node
        Node * start = &(nodes.at(n));
        std::stack<Node *> stack;
        while (true) {
            if (start->succs.size()) {
                    // # pragma omp parallel for schedule(dynamic)
		    for (unsigned int i = 0 ; i < start->succs.size() ; i++) {
                            Node * end = start->succs.at(i);
			    end->indexes = Follow::adaptativStep(
					       start->point,
                                               end->point,
                                               start->indexes,
                                               distanceThreshold,
                                               minstepsize
                                           );
		    }

                    for (auto & end : start->succs) {
                        stack.push(end);
                    }
	    }
            start->succs.clear();
            if (stack.empty()) {
                break ;
            }
            start = stack.top();
            stack.pop();
        }

        // go through the tree and fill the matrix
        ComplexMatrix mat(n, m);

        # pragma omp parallel for schedule(static)
        for (unsigned int i = 0 ; i < n ; i++) {
            Node & node = nodes[i];
            for (unsigned int j = 0 ; j < m ; j++) {
                mat(i, j) = node.point.formatedEigenvalues[node.indexes[j]];
            }
            #pragma omp critical
	    {
                outCsv.write( (ComplexVector) phaseSpeed(startPoint.omega, node.point.formatedEigenvalues) );
            }
        }

        return mat;
    }
}
