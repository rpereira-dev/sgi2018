# include "pqueue.h"

/**
 *	Structure de donnée: 'File de priorité' , ou 'Priority Queue'
 *
 *	sous forme de 'Tas binaire', 'Binary heap'
 *
 *	https://www.geeksforgeeks.org/binary-heap/
 */

/**
 *	@require: 'cmpf': fonction comparant les clefs (voir 'strcmp()')
 *	@ensure : renvoie une nouvelle file de priorité
 *	@assign : ---------------------	
 */
pqueue_t * pqueue_new(cmpf_t cmpf) {
	pqueue_t * pqueue = (pqueue_t *) malloc(sizeof(pqueue_t));
	if (pqueue == NULL) {
		return (NULL);
	}
	pqueue->nodes = array_new(16, sizeof(pqueue_node_t *));
	if (pqueue->nodes == NULL) {
		free(pqueue->nodes);
		return (NULL);
	}
	pqueue->cmpf = cmpf;
	return (pqueue);
}

/**
 *	@require: une file de priorité
 *	@ensure : supprime la file de la mémoire
 *	@assign : ---------------------
 */
void pqueue_delete(pqueue_t * pqueue) {
	ARRAY_ITERATE_START(pqueue->nodes, pqueue_node_t **, nodeRef, i) {
		free(*nodeRef);
	}
	ARRAY_ITERATE_STOP(pqueue->nodes, pqueue_node_t **, nodeRef, i);
	array_delete(pqueue->nodes);
	free(pqueue);
}

/**
 *	@require: une file de priorité
 *	@ensure : renvoie 1 si la file est vide, 0 sinon
 *	@assign : ---------------------	
 */
int pqueue_is_empty(pqueue_t * pqueue) {
	return (pqueue->nodes->size == 0);
}

/** fonction interne pour recuperer une node */
static inline pqueue_node_t * pqueue_get_node(pqueue_t * pqueue, unsigned int i) {
	return (*((pqueue_node_t **)array_get(pqueue->nodes, i)));
}

/**
 *	@require: une file de priorité, une clef, et une valeur
 *			renvoie 1 si l'élément a pu etre inseré, 0 sinon
 *	@ensure : ajoutes l'élément dans la file.
 *			- il y a copie des adresses, pas des valeurs!
 *		  	- si la clef doit être modifié, appelé 'pqueue_decrease()'
 *	@assign : --------------------------
 */
pqueue_node_t * pqueue_insert(pqueue_t * pqueue, void const * key, void const * value) {
	pqueue_node_t * node = (pqueue_node_t *) malloc(sizeof(pqueue_node_t));
	if (node == NULL) {
		return (NULL);
	}
	/** initialise le sommet de la file */
	node->key	= key;
	node->value	= value;
	node->index	= array_add(pqueue->nodes, &node);

	/** on s'assure de l'intégrité du tas binaire */

	/** le nouveau sommet ayant été inseré à la fin du tas
	    on le remonte à sa position final, afin de
	    respecter les regles du tas binaire */
	unsigned int i = pqueue->nodes->size - 1;
	while (i != 0) {
		/** parent de 'i' */
		unsigned int pi = (i - 1) / 2;
		pqueue_node_t * i_node	= pqueue_get_node(pqueue, i);
		pqueue_node_t * pi_node	= pqueue_get_node(pqueue, pi);
		/** si 'i' est plus grand que son père, c'est qu'on a
		    trouvé sa place. On s'arrête */
		if (pqueue->cmpf(i_node->key, pi_node->key) >= 0) {
			break ;
		}
		/** sinon, on remonte 'i' en l'echangeant avec son père */
		array_set(pqueue->nodes, i, &pi_node);
		array_set(pqueue->nodes, pi, &i_node);
		i_node->index = pi;
		pi_node->index = i;
		i = pi;
	}
	return (node);
}

/**
 *	@require: une file de priorité, un sommet, et une nouvelle clef plus faible
 *		  que la precedente pour ce sommet
 *	@ensure : diminue la clef d'une valeur (<=> augmente sa priorité)
 *	@assign : ----------------------
 */
void pqueue_decrease(pqueue_t * pqueue, pqueue_node_t * node, void const * newkey) {
	/** on met à jour la clef du sommet */
	node->key = newkey;

	/** sur le meme modele que 'pqueue_insert()', on remonte le tas
	    pour replacer le sommet au bon endroit */
	unsigned int i = node->index;
	while (i != 0) {
		unsigned int pi = (i - 1) / 2;
		pqueue_node_t * i_node	= pqueue_get_node(pqueue, i);
		pqueue_node_t * pi_node	= pqueue_get_node(pqueue, pi);
		if (pqueue->cmpf(pi_node->key, i_node->key) < 0) {
			break ;
		}
		array_set(pqueue->nodes, i, &pi_node);
		array_set(pqueue->nodes, pi, &i_node);
		i_node->index = pi;
		pi_node->index = i;
		i = pi;
	}
}

/**
 *	@require: une file de priorité
 *	@ensure: renvoie l'élément avec la clef la plus basse (<=> la plus haute priorité)
 *	@assign: --------------------------
 */
pqueue_node_t * pqueue_minimum(pqueue_t * pqueue) {
	return (pqueue_get_node(pqueue, 0));
}

/**
 *	fonction interne qui assure l'intégrité de la structure de manière
 *	recursive, en partant de l'index 'i', puis en redescendant dans
 *	le tas
 */
static void pqueue_heapify(pqueue_t * pqueue, unsigned int i) {
	array_t * nodes = pqueue->nodes;
	unsigned int l = 2 * i + 1;
	unsigned int r = 2 * i + 2;
	unsigned int s = i; /** plus petite clef entre 'i', 'l', et 'r' */
	pqueue_node_t * inode = pqueue_get_node(pqueue, i);

	if (l < nodes->size) {
		pqueue_node_t * lnode = pqueue_get_node(pqueue, l);
		if (pqueue->cmpf(lnode->key, inode->key) < 0) {
			s = l;
		}
	}

	if (r < nodes->size) {
		pqueue_node_t * rnode = pqueue_get_node(pqueue, r);
		pqueue_node_t * snode = pqueue_get_node(pqueue, s);
		if (pqueue->cmpf(rnode->key, snode->key) < 0) {
			s = r;
		}
	}

	if (s != i) {
		pqueue_node_t * snode = pqueue_get_node(pqueue, s);

		array_set(pqueue->nodes, i, &snode);
		array_set(pqueue->nodes, s, &inode);
		inode->index = s;
		snode->index = i;
		pqueue_heapify(pqueue, s);
	}
}

/**
 *	@require: une file de priorité
 *	@ensure: supprime et renvoie l'élément de la file avec
 *		 la clef la plus basse (<=> la plus haute priorité)
 *	@assign: --------------------------
 */
pqueue_node_t pqueue_pop(pqueue_t * pqueue) {
	if (pqueue->nodes->size == 0) {
                pqueue_node_t node = {NULL, NULL, (unsigned int)-1};
		return (node);
	}

	/** sauvegarde le minimum */
	pqueue_node_t * min = pqueue_minimum(pqueue);

	array_t * nodes = pqueue->nodes;

	/** si c'est le seul sommet, on le retire */
	if (nodes->size == 1) {
		array_remove(nodes, 0);
	} else {
	/** sinon, on le retire et on retri le tas */
		pqueue_node_t * last = pqueue_get_node(pqueue, nodes->size - 1);
		last->index = 0;
		array_set(pqueue->nodes, 0, &last);
		array_removelast(nodes);
		pqueue_heapify(pqueue, 0);
	}

	/** renvoie une copie */
        pqueue_node_t node = *min;
	free(min);
	return (node);
}

/*
static int intcmp(int * a, int * b) {
	if (*a < *b) {
		return (-1);
	}
	if (*a > *b) {
		return (1);
	}
	return (0);
}

#include <stdlib.h>
#include <time.h>
int main() {
	pqueue_t * pqueue = pqueue_new((cmpf_t)intcmp);

	srand(time(NULL));
	rand();

	int n = 4096;
	pqueue_node_t ** nodes = (pqueue_node_t **)malloc(sizeof(pqueue_node_t *) * n);
	int i;
	for (i = 0 ; i < n ; i++) {
		int * key = (int *)malloc(sizeof(int));
		int * value = (int *) malloc(sizeof(int));
		*key = rand();
		*value = rand();
		nodes[i] = pqueue_insert(pqueue, key, value);
	}

	for (i = 0 ; i < n ; i++) {
		pqueue_node_t * node = nodes[i];
		int * oldkeyRef = (int *) node->key;
		int newkey = rand();
		if (newkey < *oldkeyRef) {
			int * newkeyRef = (int*) malloc(sizeof(int));
			*newkeyRef = newkey;
			pqueue_decrease(pqueue, node, newkeyRef);
			free(oldkeyRef);
		}
	}

	for (i = 0 ; i < n ; i++) {
		pqueue_node_t node = pqueue_pop(pqueue);
		int * key = (int *)node.key;
		int * value = (int *)node.value;
		printf("%d (%d)\n", *key, *value);
		free(key);
		free(value);
	}
	free(nodes);
	pqueue_delete(pqueue);
	return (0);
}
*/
