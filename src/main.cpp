
#include <iostream>
#include "types.h"
#include "tools/csv.h"
#include "follower/point.h"
#include "follower/transform.h"

//-------------------------------------------------------------------------------------------------
// TARGETS

// formated eigenvalues of interest for omega = 10Hz
auto formatedTargetEigenvalues_10Hz = []()
{
    std::vector<complexNumber> formatedTargetEigenvalues_10Hz =
    {
        complexNumber(0.35561, 2.8487e-05),
        complexNumber(0.354, 2.848e-05),
    };

    return formatedTargetEigenvalues_10Hz;
};

// formated eigenvalues of interest for omega = 20Hz
auto formatedTargetEigenvalues_20Hz = []()
{
    std::vector<complexNumber> formatedTargetEigenvalues_20Hz;
    if(Generator::use_constant_continuation)
    {
        formatedTargetEigenvalues_20Hz =
        {
            complexNumber(3.6103700000000000e-01,   2.8933800000000001e-05),
            complexNumber(3.5561300000000001e-01,   2.8487699999999998e-05),
            complexNumber(3.5397600000000001e-01,   2.8485500000000000e-05),
            complexNumber(3.5202299999999997e-01,   2.8547700000000001e-05),
            complexNumber(3.4986499999999998e-01,   2.8571900000000000e-05),
            complexNumber(3.4782600000000002e-01,   2.8344600000000000e-05),
            complexNumber(3.4604499999999999e-01,   2.7885000000000002e-05),
            complexNumber(3.4399000000000002e-01,   2.8099100000000000e-05),
            complexNumber(3.4109699999999998e-01,   2.8376100000000001e-05)
        };
    }
    else
    {
        formatedTargetEigenvalues_20Hz =
        {
            complexNumber(0.361033647187131, 0.000028933251563),
            complexNumber(0.355614091880504, 0.000028486937552),
            complexNumber(0.353997839239009, 0.000028480251433),
            complexNumber(0.352101292441200, 0.000028478101360),
            complexNumber(0.348275599694248, 0.000028517568097),
            complexNumber(0.352097123839055, 0.000028504433229),
            complexNumber(0.352629811458154, 0.000028414037120),
            complexNumber(0.353112905615286, 0.000028382664958),
            complexNumber(0.353546771365575, 0.000028353869735)
        };
    }

    return formatedTargetEigenvalues_20Hz;
};

//-------------------------------------------------------------------------------------------------
// MAIN

int main(int argc, char *argv[])
{
    std::cout << "Starting..." << std::endl;
    const number omega = 20*2*pi; // 20Hz
    const int N_gPC = 1000;
    const unsigned int size = (argc > 1) ? atoi(argv[1]) : 50;
    const std::string outPutfolder = Csv::confirmedFolder("output/");
    Generator::use_constant_continuation = false; // makes eigenvalues easier to follow ?

    std::cout << "Defining target eigenvalues... (omega=" << omega << ')' << std::endl;

    const Point startingPoint(omega, size);
    const std::vector<complexNumber> targetformatedEigenValues = formatedTargetEigenvalues_20Hz();
    const std::vector<unsigned int> indexTargets = startingPoint.findClosest(targetformatedEigenValues).first;

    std::cout << "Generating " << N_gPC << " random points..." << std::endl;

    const ComplexVector xiVector = randn(N_gPC);
    Csv::write(outPutfolder + "xi.csv", xiVector);

    std::cout << "MONTE-CARLO: Following target with raw computation..." << std::endl;

    Csv allEigenvaluesCsv(outPutfolder + "phase_eigenvalues.csv"); // to dump the phases of all the eigenvalues we meet
    const ComplexMatrix eigenvaluesMC = Transform::linearPath(startingPoint, indexTargets, xiVector, allEigenvaluesCsv);
    //const ComplexMatrix eigenvaluesMC = Transform::treePath(startingPoint, indexTargets, xiVector, allEigenvaluesCsv);
    Csv::write(outPutfolder + "phase_montecarlo.csv", phaseSpeed(omega, eigenvaluesMC));

    return 0;
}
